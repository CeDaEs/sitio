#!/usr/bin/env python
# -*- coding: utf-8 -*- #

AUTHOR = 'Renich Bon Ćirić'
SITENAME = 'Centros de Datos Escuela'
SITEURL = 'http://localhost:8000'

PATH = 'content'

TIMEZONE = 'America/Mexico_City'

DEFAULT_LANG = 'es'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Inicio', '/'),
         ('Acerca', '/pages/acerca'),
         ('Contacto', '/pages/contacto'))

# Social widget
SOCIAL = (('Se pueden agregar ligas sociales', '#'),
          ('Otra acá.', '#'),)

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

# theme
THEME = './themes/voce/'
DEFAULT_DATE_FORMAT = "%d %b, %Y"
USER_LOGO_URL = "/images/logo.png"

# archives
TAGS_URL = 'tags.html'
ARCHIVES_URL = 'archives.html'

