Actualizaciones al sitio
========================

:date: 2022-10-27
:category: General
:tags: Noticias
:Slug: actualizaciones-al-sitio

Por ahora, se ha renovado el sitio utilizando el tema "voce"; diseñado por Benjamin Lim.

La idea es tener un sitio más limpio y preparado para hospedar recursos variados, como:

* noticias.
* páginas.
* guías.
* etc.

Es solo otra prueba por lo pronto.
